from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone


class TipoEstadoReporte(models.Model):
    nombre = models.CharField(max_length=25)
    descripcion = models.TextField()
    estado = models.BooleanField(default=True)

    def __unicode__(self):
        return self.nombre


class Prioridad(models.Model):
    nombre = models.CharField(max_length=25)
    valor = models.IntegerField(default=1)
    descripcion = models.TextField(null=True, blank=True)

    def __unicode__(self):
        return self.nombre


class TipoReporte(models.Model):
    nombre = models.CharField(max_length=25)
    prioridad = models.ForeignKey(Prioridad)
    estado = models.BooleanField(default=True)

    def __unicode__(self):
        return self.nombre


class Reporte(models.Model):
    usuario = models.ForeignKey(User)
    tipo_reporte = models.ForeignKey(TipoReporte)
    latitud = models.CharField('latitud', max_length=25)
    longitud = models.CharField('longitud', max_length=25)
    fecha_crecion = models.DateTimeField(default=timezone.now)
    fecha_finalizacion = models.DateTimeField(null=True, blank=True)
    comentario = models.TextField(null=True, blank=True)
    img_reporte = models.ImageField(upload_to = 'images/')
    confirmaciones = models.IntegerField(default=1)

    def __unicode__(self):
        return self.tipo_reporte.nombre

    def get_tipo_reporte(self):
        return self.tipo_reporte.nombre

    get_tipo_reporte.short_description = 'tipo de reporte'


class EstadoReporte(models.Model):
    reporte = models.ForeignKey(Reporte)
    tipo_estado_reporte = models.ForeignKey(TipoEstadoReporte)
    estado = models.BooleanField(default=True)
    fecha_inicio = models.DateTimeField(default=timezone.now)
    fecha_finalizacion = models.DateTimeField(null=True, blank=True)

    def __unicode__(self):
        return self.reporte.tipo_reporte.nombre
        '{}{}'.format(self.estado, self.fecha_inicio)


class Confirmaciones(models.Model):
    reporte = models.ForeignKey(Reporte, related_name='rconfirmaciones')
    usuario = models.ForeignKey(User)
    fecha = models.DateTimeField(default=timezone.now)
    estado = models.BooleanField(default=True)
