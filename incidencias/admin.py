from django.contrib import admin

from .models import *


class ReporteAdmin(admin.ModelAdmin):
    list_display = ['usuario', 'get_tipo_reporte', 'latitud', 'longitud' '']


admin.site.register(Reporte, ReporteAdmin)
admin.site.register(TipoEstadoReporte)
admin.site.register(Prioridad)
admin.site.register(TipoReporte)
admin.site.register(EstadoReporte)
admin.site.register(Confirmaciones)
