from django.apps import AppConfig


class SocialouthConfig(AppConfig):
    name = 'socialouth'
